#include <QFileDialog>
#include <QDebug>
#include <QDateTime>

#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(&mThread, &GeneticThread::newGenerationReady,
            this, &MainWindow::on_newGeneration);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_newGeneration(std::vector<uint> *data)
{
    qDebug() << "New data received. Len: " << data->size() << ".";
    mBestRaw = *data;
    updateBestPreview();
}

void MainWindow::on_imageSelectButton_clicked()
{
    qDebug() << "Image select clicked!";
    QFileDialog imgFile(this);
    QString imgPath = imgFile.getOpenFileName();
    qDebug() << "File selected: " << imgPath;
    QPixmap imgPixmap(imgPath);
    mTargetImage = imgPixmap.toImage().convertToFormat(QImage::Format_Grayscale8);
    mTargetImage = mTargetImage.convertToFormat(QImage::Format_ARGB32);
    mBestImage = mTargetImage;
    //mBestRaw.resize(static_cast<int>(mBestImage.sizeInBytes()));
    //mBestRaw.fill(static_cast<char>(0x55));
    mBestRaw.assign(static_cast<size_t>(mBestImage.sizeInBytes()/4), 0x555555FF);
    ui->InputPreview->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    ui->InputPreview->setPixmap(imgPixmap);
    ui->NormalizedPreview->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    ui->NormalizedPreview->setPixmap(QPixmap::fromImage(mTargetImage));
    ui->BestPreview->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    updateBestPreview();
}

void MainWindow::updateBestPreview()
{
    for(int h = 0; h < mBestImage.height(); h++) {
        for(int w = 0; w < mBestImage.width(); w++) {
            size_t idx = static_cast<size_t>(h*mBestImage.width() + w);
//            tmp |= (static_cast<uint>(mBestRaw[idx+1]<<16))&0xff0000;
//            tmp |= (static_cast<uint>(mBestRaw[idx+2]<<8))&0xff00;
//            tmp |= (static_cast<uint>(mBestRaw[idx+3]))&0xff;
            mBestImage.setPixel(w, h, mBestRaw[idx]);
        }
    }
    ui->BestPreview->setPixmap(QPixmap::fromImage(mBestImage));
}

void MainWindow::on_startButton_clicked()
{
    mThread.setImage(static_cast<uint>(mBestImage.width()),
                     static_cast<uint>(mBestImage.height()),
                     &mTargetImage);
    mThread.setStart(true);
    mThread.start();
}

void MainWindow::on_stopButton_clicked()
{
    mThread.setStart(false);
}

void GeneticThread::setImage(uint width, uint height, QImage *img)
{
    mWidth = width;
    mHeigh = height;
    mTarget = img;
    for (auto &speciman: mSpecimen) {
        speciman.data.assign(width*height, 0xFF000000u);
    }
}

void GeneticThread::run()
{
    int counter = 0;
    while (mRunning && (mTarget != nullptr)) {
        QDateTime dateTime1 = QDateTime::currentDateTime();
        mutate();
        score();
        cross();
        QDateTime dateTime2 = QDateTime::currentDateTime();
        qDebug() << "Round Timediff: " << dateTime1.msecsTo(dateTime2) << " ms";
        if (counter%10 == 0) {
            qDebug() << "Thread running " << counter << "...";
            //mBuffer = QByteArray(reinterpret_cast<char *>(mSpecimen[0].data.data()), static_cast<int>(mSpecimen[0].data.size()*4));
            emit newGenerationReady(&mSpecimen[0].data);
        }
        counter++;
    }
    qDebug() << "Thread stopped.";
}

void GeneticThread::mutate()
{
    for (size_t idx = 10; idx < mMAX_SPECIMEN; idx++) {
        auto &speciman = mSpecimen[idx];
        uint tmpX = static_cast<uint>(rand()) % mWidth;
        uint tmpY = static_cast<uint>(rand()) % mHeigh;
        uint tmpW = (static_cast<uint>(rand()) % (mWidth - tmpX))/3;
        uint tmpH = (static_cast<uint>(rand()) % (mHeigh - tmpY))/3;
        uint32_t tmpVal = static_cast<uint32_t>(rand() % 256);
        //uint32_t tmpPixel = (tmpVal<<24) | (tmpVal<<16) | (tmpVal<<8) | 0xFF;
        uint32_t tmpPixel = 0xFF000000 | (tmpVal<<16) | (tmpVal<<8) | (tmpVal);
        for (size_t jdx = 0; jdx < speciman.data.size(); jdx++) {
            auto curX = jdx%(mWidth);
            auto curY = jdx/(mWidth);
            if ((curX >= tmpX) && (curX < (tmpX+tmpW)) &&
                (curY >= tmpY) && (curY < (tmpY+tmpH)) ) {
                speciman.data[jdx] = tmpPixel;
            }
        }
//        for (auto h = 0u; h < tmpH; h++) {
//            for (auto w = 0u; w < tmpW; w++) {
//                size_t offset = 4*(mWidth*(tmpY+h)+tmpX+w);
//                uint8_t tmpNew = tmpVal;//(speciman.data[offset+1]*5+tmpVal)/6;
//                speciman.data[offset+1] = tmpNew;
//                speciman.data[offset+2] = tmpNew;
//                speciman.data[offset+3] = tmpNew;
//            }
//        }
    }
}

int GeneticThread::compareGenetic(const void *a, const void *b)
{
    const GeneticData *aa = static_cast<const GeneticData *>(a);
    const GeneticData *bb = static_cast<const GeneticData *>(b);
    if (aa->score < bb->score) {
        return -1;
    }
    return 1;
}

void GeneticThread::score()
{
    for (auto &speciman: mSpecimen) {
        speciman.score = 0.0;
        for (uint h = 0; h < mHeigh; h++) {
            for (uint w = 0; w < mWidth; w++) {
                uint offset = (mWidth*h+w);
                int diff = static_cast<int>(mTarget->bits()[offset*4+1]);
                diff -= static_cast<int>(speciman.data[offset]&0x0FF);
                if (diff < 0) {
                    diff = diff*(-1);
                }
                speciman.score += diff;
            }
        }
    }
}

void GeneticThread::cross()
{
    qsort(mSpecimen, mMAX_SPECIMEN, sizeof(GeneticData), GeneticThread::compareGenetic);
    for (size_t idx = 10; idx < mMAX_SPECIMEN; idx++) {
        mSpecimen[idx].data.resize(0);
        mSpecimen[idx] = mSpecimen[idx%10];
    }
}

GeneticThreadCL::GeneticThreadCL()
{
    qDebug() << "Genetic Picture Generation Demo." << endl;
    std::vector<cl::Platform> platforms;
    auto err = cl::Platform::get(&platforms);

    if (err != CL_SUCCESS) {
        qDebug() << "Error: " << err << endl;
        return;
    }

    std::vector<cl::Device> gpuDevices;

    for (auto platform : platforms) {
        std::vector<cl::Device> platDevs;
        err = platform.getDevices(CL_DEVICE_TYPE_GPU, &platDevs);
        if (err == CL_SUCCESS) {
            for (auto device : platDevs) {
                gpuDevices.push_back(device);
            }
        }
    }

    if (gpuDevices.size() == 0) {
        qDebug() << "Failed to find any GPU device." << endl;
        return;
    }

    auto gpuDevice = gpuDevices[0];

    auto tmpDeviceName = gpuDevice.getInfo<CL_DEVICE_NAME>().data();
    qDebug() << "Device name:       " << tmpDeviceName;
    auto tmpDeviceAvailable = gpuDevice.getInfo<CL_DEVICE_AVAILABLE>();
    qDebug() << "Device available:  " << tmpDeviceAvailable;
    mGpuContext = cl::Context(gpuDevice);
    mQueue = cl::CommandQueue(mGpuContext, gpuDevice);
    mProgramMutate = cl::Program(mGpuContext, mSource_mutate);
    err = mProgramMutate.build();
    if (err != CL_SUCCESS) {
        qDebug() << "Failed to build program. Error: " << err;
        return;
    }
    mProgramScore = cl::Program(mGpuContext, mSource_score);
    err = mProgramScore.build();
    if (err != CL_SUCCESS) {
        qDebug() << "Failed to build program. Error: " << err;
        return;
    }
    mMutate = cl::Kernel(mProgramMutate, "mutate");
}

void GeneticThreadCL::mutate()
{
    for (size_t idx = 10; idx < mMAX_SPECIMEN; idx++) {
        auto &speciman = mSpecimen[idx];
        uint tmpX = static_cast<uint>(rand()) % mWidth;
        uint tmpY = static_cast<uint>(rand()) % mHeigh;
        uint tmpW = (static_cast<uint>(rand()) % (mWidth - tmpX))/3;
        uint tmpH = (static_cast<uint>(rand()) % (mHeigh - tmpY))/3;
        uint tmpVal = static_cast<uint8_t>(rand() % 256);
        uint tmpPixel = 0xFF000000 | (tmpVal<<16) | (tmpVal<<8) | (tmpVal);
        cl::Buffer OUT(mGpuContext, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR /*CL_MEM_COPY_HOST_PTR*/, speciman.data.size()*sizeof(uint), speciman.data.data());
        mMutate.setArg(0, static_cast<cl_ulong>(tmpX));
        mMutate.setArg(1, static_cast<cl_ulong>(tmpY));
        mMutate.setArg(2, static_cast<cl_ulong>(tmpW));
        mMutate.setArg(3, static_cast<cl_ulong>(tmpH));
        mMutate.setArg(4, static_cast<cl_ulong>(mWidth));
        mMutate.setArg(5, static_cast<cl_ulong>(mHeigh));
        mMutate.setArg(6, static_cast<cl_uint>(tmpPixel));
        mMutate.setArg(7, OUT);
        mQueue.enqueueNDRangeKernel(mMutate, cl::NullRange, speciman.data.size(), cl::NullRange);
        mQueue.finish();
        mQueue.enqueueReadBuffer(OUT, CL_TRUE, 0, speciman.data.size()*sizeof(uint), speciman.data.data());
    }
}

void GeneticThreadCL::score()
{
    for (auto &speciman: mSpecimen) {
        speciman.score = 0.0;
        for (uint h = 0; h < mHeigh; h++) {
            for (uint w = 0; w < mWidth; w++) {
                uint offset = (mWidth*h+w);
                int64_t diff = static_cast<uint>(mTarget->bits()[offset*4]);
                diff -= static_cast<int>(speciman.data[offset]&0x0FF);
                if (diff < 0) {
                    diff = diff*(-1);
                }
                speciman.score += diff;
            }
        }
    }
}
