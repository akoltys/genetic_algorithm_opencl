#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>

#include <vector>

#include "CL/cl2.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class GeneticThread : public QThread
{
    Q_OBJECT

public:
    void setStart(bool start) { mRunning = start; }
    void setImage(uint width, uint height, QImage *img);
    static int compareGenetic (const void * a, const void * b);

protected:
    bool mRunning = false;
    uint mWidth = 0;
    uint mHeigh = 0;
    QImage *mTarget = nullptr;
    QByteArray mBuffer;
    static const size_t mMAX_SPECIMEN = 100;
    static const size_t mBEST_COUNT = 10;
    struct GeneticData {
        std::vector<uint32_t> data;
        double  score;
    };
    GeneticData mSpecimen[mMAX_SPECIMEN];

    void run();

private:

    virtual void mutate();
    virtual void score();
    void cross();

signals:
    void newGenerationReady(std::vector<uint> *data);
};

class GeneticThreadCL final: public GeneticThread {
public:
    explicit GeneticThreadCL();
private:
    static constexpr char mSource_mutate[] =
"#if defined(cl_khr_fp64)\n"
"#  pragma OPENCL EXTENSION cl_khr_fp64: enable\n"
"#elif defined(cl_amd_fp64)\n"
"#  pragma OPENCL EXTENSION cl_amd_fp64: enable\n"
"#else\n"
"#  error double precision is not supported\n"
"#endif\n"
"kernel void mutate(\n"
"       ulong x,\n"
"       ulong y,\n"
"       ulong w,\n"
"       ulong h,\n"
"       ulong mw,\n"
"       ulong mh,\n"
"       uint val,\n"
"       global uint* out\n"
"       )\n"
"{\n"
"    size_t i = get_global_id(0);\n"
"    ulong curX = i%mw;\n"
"    ulong curY = i/mw;\n"
"    if ((curX >= x) && (curX < (x+w)) &&\n"
"        (curY >= y) && (curY < (y+h))) {\n"
"       out[i] = val;\n"
"    }\n"
"}\n";

    static constexpr char mSource_score[] =
    "#if defined(cl_khr_fp64)\n"
    "#  pragma OPENCL EXTENSION cl_khr_fp64: enable\n"
    "#elif defined(cl_amd_fp64)\n"
    "#  pragma OPENCL EXTENSION cl_amd_fp64: enable\n"
    "#else\n"
    "#  error double precision is not supported\n"
    "#endif\n"
    "kernel void add(\n"
    "       ulong n,\n"
    "       global const double *a,\n"
    "       global const double *b,\n"
    "       global double *c\n"
    "       )\n"
    "{\n"
    "    size_t i = get_global_id(0);\n"
    "    if (i < n) {\n"
    "       c[i] = a[i] + b[i];\n"
    "    }\n"
    "}\n";


    cl::Context mGpuContext;//(gpuDevice);
    cl::CommandQueue mQueue;//(gpuContext, gpuDevice);
    cl::Program mProgramMutate;//(gpuContext, source);
    cl::Program mProgramScore;//(gpuContext, source);
    cl::Kernel mMutate;

    void mutate() override;
    void score() override;
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_newGeneration(std::vector<uint> *data);

    void on_imageSelectButton_clicked();

    void on_startButton_clicked();

    void on_stopButton_clicked();

private:
    Ui::MainWindow *ui;
    QImage mTargetImage;
    QImage mBestImage;
    std::vector<uint> mBestRaw;
    GeneticThreadCL mThread;

    void updateBestPreview();
};

#endif // MAINWINDOW_H
